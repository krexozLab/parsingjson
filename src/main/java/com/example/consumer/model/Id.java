package com.example.consumer.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Id {
    private Integer id;
}
