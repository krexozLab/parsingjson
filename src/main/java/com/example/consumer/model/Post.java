package com.example.consumer.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Post implements Serializable {
    private Integer userId;
    private Integer id;
    private String title;
    private String body;

}
