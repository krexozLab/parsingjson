package com.example.consumer.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.consumer.model.Id;
import com.example.consumer.model.Post;
import com.example.consumer.service.RestService;

@RestController
@RequestMapping("/api/v1")
public class MainController {

    @Autowired
    RestService restService;

    @PostMapping("/post")
    public Post getOnePostByPost(@RequestBody Id id){
        Post postById = this.restService.getPostById(id);
        return postById;
    }

    @GetMapping("/posts")
    public List<Post> getPosts(){
        final Post[] posts = this.restService.getPostsAsObject();
        final List<Post> list = Arrays.asList(posts);
        return list;
    }

    @GetMapping("/posts/{id}")
    public Post getOnePostById (@PathVariable int id) {
        final Post postById = this.restService.getPostWithUrlParameters(id);
        return postById;
    }
}
