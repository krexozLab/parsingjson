package com.example.consumer.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IndexOutOfBoundsException.class)
    @ResponseBody
    public ResponseEntity<?> handleIndexOutOfBoundsException(IndexOutOfBoundsException e,
                                                            WebRequest request){
        ExceptionDetails exceptionDetails = new ExceptionDetails(new Date(),
                "Sorry but your index it's out of bound. ( 1 - 100 )",
                request.getDescription(false));

        return new ResponseEntity(exceptionDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex,
                                                        WebRequest request) {
        ExceptionDetails exceptionDetails = new ExceptionDetails(new Date(),
                "Please don't leave your request body empty. / User a valid input",
                request.getDescription(false));

        return new ResponseEntity(exceptionDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseBody
    public ResponseEntity<?> handleHttpClientErrorException(HttpClientErrorException ex,
                                                            WebRequest request){
        ExceptionDetails exceptionDetails = new ExceptionDetails(new Date(),
                "The link you're trying to access doesn't exist. Try another value or endpoint",
                request.getDescription(false));

        return new ResponseEntity(exceptionDetails, HttpStatus.BAD_REQUEST);
    }
}
