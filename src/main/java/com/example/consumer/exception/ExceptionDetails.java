package com.example.consumer.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class ExceptionDetails {
    private Date timestamp;
    private final String message;
    private String details;
}
