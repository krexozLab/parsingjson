package com.example.consumer.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.consumer.model.Id;
import com.example.consumer.model.Post;

@Service
public class RestService {

    private static final String POST_URL = "https://jsonplaceholder.typicode.com/posts";
    private static final String FORMAT_POSTS_URL = "https://jsonplaceholder.typicode.com/posts/%s";

    private final RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public Post[] getPostsAsObject() {
        return this.restTemplate.getForObject(POST_URL, Post[].class);
    }

    public Post getPostWithUrlParameters(int id) {
        return this.restTemplate.getForObject(
                String.format(FORMAT_POSTS_URL, id),
                Post.class,
                id);
    }

    public Post getPostById(Id id) {
        final Post[] posts = this.getPostsAsObject();
        final List<Post> list = Arrays.asList(posts);
        final Post postObject = list.stream()
                .filter(post -> post.getId().equals(id.getId()))
                .collect(Collectors.toList())
                .get(0);
        return postObject;
    }
}