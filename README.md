# MyApplication 1.0

## Welcome to my first project :D

Using Spring Boot RESTful Web Service.

## Used to parse Json from the :

 > https://jsonplaceholder.typicode.com/posts

GET METHOD AT : localhost:8090/api/v1/posts ( for all posts )

GET METHOD AT : localhost:8090/api/v1/posts/{id} ( for a specific post )

POST METHOD AT : localhost:8090/api/v1/post ( for getting a specific post by @RequestBody )


 > Dependencies:
 > - 
 > - Web Service
 > - Actuator [ health, metrics ]   
 > - Lombok    
 > - Thymeleaf